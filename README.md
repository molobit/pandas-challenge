# PyCitySchools Analysis

In the current school data under analysis we study 15 schools, with a total of 39,170 students and a combined budget of $24,649,428.00 USD.
General information about the district shows that approximately 65% of students are passing both reading and math, 74.98% passing math and 85.80% passing reading.

Data shows that the top five overall performing schools are Charter Schools and the bottom performing are District Schools.

Also, it appears like schools with higher spending per students are also with the least number of overall passing students.

